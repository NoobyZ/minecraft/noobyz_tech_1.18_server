// priority: 100

onEvent('recipes', e => {

    // Remove Recipes
    e.remove({output: 'quark:apple_crate',})
    e.remove({output: 'quark:potato_crate',})
    e.remove({output: 'quark:carrot_crate',})
    e.remove({output: 'quark:beetroot_crate',})
    e.remove({output: 'quark:charcoal_block',})
    e.remove({output: 'quark:gunpowder_sack',})
})
