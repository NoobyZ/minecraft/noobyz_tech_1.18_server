//priority: 1000

//#region ITEM TAGS
onEvent('item.tags', e => {
    e.add('minecraft:wart_blocks', '/.+_wart_block/')
    e.add('minecraft:stone_tool_materials', ['#forge:stone', '#forge:cobblestone'])
    e.add('minecraft:stone_crafting_materials', ['#forge:stone', '#forge:cobblestone'])
    e.add('minecraft:logs_unstripped', '/^.+:(?!stripped_).+_log$/')

    e.add('forge:plastic', 'mekanism:hdpe_sheet')
    e.add('forge:seeds', 'immersiveengineering:seed')
    e.add('forge:gems', '#forge:gems/dimensionalshard')
    e.add('forge:gems/dimensionalshard', 'rftoolsbase:dimensionalshard')
    e.add('forge:axes', ['/.+_axe/', '/.+_paxel/', '/.+:axe_.+/'])
    e.add('forge:pickaxes', ['/.+_pickaxe/', '/.+_paxel/', '/.+:pickaxe_.+/'])
    e.add('forge:shovels', ['/.+_shovel/', '/.+_paxel/', '/.+:shovel_.+/'])
    e.add('forge:swords', ['/.+_sword/', '/.+:sword_.+/'])
    e.add('forge:hoes', ['/.+_hoe/', '/.+:hoe_.+/'])
    e.add('forge:simple_honeycomb', '/^.+:(?!(catnip|rgbee)_).*honeycomb$/')
    e.add('forge:simple_honeycomb/blocks', '/^.+:(?!(catnip|rgbee)_).*honeycomb_block$/')
    e.add('forge:fertilizer', ['mysticalagriculture:mystical_fertilizer', 'thermal:phytogro'])

    e.add('forge:mushroom_caps/brown_mushroom', 'minecraft:brown_mushroom_block')
    e.add('forge:mushroom_caps/red_mushroom', 'minecraft:red_mushroom_block')
    e.add('forge:mushroom_stems/mushroom', 'minecraft:mushroom_stem')
    e.add('forge:mushroom_caps', ['#forge:mushroom_caps/red_mushroom', '#forge:mushroom_caps/brown_mushroom'])
    e.add('forge:mushroom_stems', '#forge:mushroom_stems/mushroom')
    e.add('forge:mushrooms', ['minecraft:warped_fungus', 'minecraft:crimson_fungus'])
    e.add('forge:ores', ['#forge:ores/dimensionalshard', '#forge:ores/draconium'])
    e.add('forge:ores/dimensionalshard', ['rftoolsbase:dimensionalshard_overworld', 'rftoolsbase:dimensionalshard_nether', 'rftoolsbase:dimensionalshard_end'])
    e.add('forge:ores/draconium', 'draconicevolution:deepslate_draconium_ore')

    e.add('misctags:flowers/draconic_flowers', 'minecraft:dragon_egg')
    e.add('misctags:flowers/end_flowers', ['minecraft:chorus_flower', 'minecraft:chorus_plant'])
    e.add('misctags:flowers/forest_flowers', ['#minecraft:flowers', 'minecraft:sweet_berries'])
    e.add('misctags:flowers/glowing_flowers', ['minecraft:shroomlight', 'minecraft:glowstone', 'minecraft:redstone_lamp'])
    e.add('misctags:flowers/nether_flowers', ['minecraft:crimson_fungus', 'minecraft:warped_fungus', 'minecraft:nether_wart', 'minecraft:crimson_roots', 'minecraft:warped_roots', 'minecraft:weeping_vines', 'minecraft:twisting_vines'])
    e.add('misctags:flowers/swamp_flowers', ['minecraft:lily_pad', 'minecraft:sugar_cane', 'minecraft:brown_mushroom', 'minecraft:red_mushroom'])
    e.add('misctags:flowers/wither_flowers', 'minecraft:wither_rose')
    e.add('misctags:water/items', ['#forge:water', 'minecraft:water_bucket', 'mysticalagriculture:water_essence'])
    e.add('misctags:immersive_engineering_hammer', 'immersiveengineering:hammer')
    e.add('misctags:immersive_engineering_wirecutter', 'immersiveengineering:wirecutter')
    e.add('misctags:biofuel2', ['#minecraft:saplings', '#minecraft:leaves', '#forge:seeds', 'minecraft:dried_kelp', 'minecraft:kelp', 'minecraft:seagrass', 'minecraft:grass'])
    e.add('misctags:biofuel4', ['#forge:fruits', 'minecraft:tall_grass', 'minecraft:nether_sprouts', 'minecraft:dried_kelp_block', 'minecraft:cactus', 'minecraft:sugar_cane', 'minecraft:weeping_vines', 'minecraft:twisting_vines', 'minecraft:vine', 'minecraft:melon_slice'])
    e.add('misctags:biofuel5', ['#forge:vegetables', '#forge:cookies', '#forge:flour_plants', '#forge:mushrooms', '#forge:mushroom_stems', '#minecraft:flowers', 'minecraft:lily_pad', 'minecraft:sea_pickle', 'minecraft:shroomlight', 'minecraft:large_fern', 'minecraft:fern', 'minecraft:crimson_roots', 'minecraft:warped_roots', 'minecraft:carved_pumpkin', 'minecraft:nether_wart', 'minecraft:cocoa_beans'])
    e.add('misctags:biofuel7', ['#minecraft:wart_blocks', '#forge:mushroom_caps', 'minecraft:baked_potato', 'minecraft:hay_block', '#forge:bread'])
    e.add('misctags:biofuel8', ['minecraft:cake', 'minecraft:pumpkin_pie'])
})

//BLOCK TAGS
onEvent('block.tags', e => {

    e.add('forge:ores', ['#forge:ores/certus_quartz', '#forge:ores/dimensionalshard', /gobber2:gobber2_ore/, /gobber2:gobber2_lucky_block/, /bigreactors:(anglesite|benitoite)_ore/, /mysticalagradditions:(nether|end)_prosperity_ore/])
    e.add('forge:ores/dimensionalshard', ['rftoolsbase:dimensionalshard_overworld', 'rftoolsbase:dimensionalshard_nether', 'rftoolsbase:dimensionalshard_end'])
    e.add('forge:ores_in_ground/stone', ['gobber2:gobber2_ore', 'gobber2:gobber2_lucky_block', 'mysticalagriculture:prosperity_ore', 'mysticalagriculture:inferium_ore', 'bigreactors:yellorite_ore'])
    e.add('forge:ores_in_ground/deepslate', ['gobber2:gobber2_ore_deepslate', 'gobber2:gobber2_lucky_block_deepslate', 'mysticalagriculture:deepslate_prosperity_ore', 'mysticalagriculture:deepslate_inferium_ore'])
    e.add('forge:ores_in_ground/netherrack', ['gobber2:gobber2_ore_nether', 'gobber2:gobber2_lucky_block_nether', 'mysticalagradditions:nether_prosperity_ore', 'mysticalagradditions:nether_inferium_ore', 'rftoolsbase:dimensionalshard_nether', 'bigreactors:benitoite_ore'])
    e.add('forge:ores_in_ground/end_stone', ['rftoolsbase:dimensionalshard_end', 'bigreactors:anglesite_ore', 'gobber2:gobber2_ore_end', 'mysticalagradditions:end_prosperity_ore', 'mysticalagradditions:end_inferium_ore'])
    e.add('forge:ores/prosperity', [/mysticalagradditions:(nether|end)_prosperity_ore/])
    e.add('forge:ores/inferium', [/mysticalagradditions:(nether|end)_inferium_ore/])

    e.add('misctags:no_moving', ['#minecraft:wither_immune', 'cookingforblockheads:fridge', /^refinedstorage:/, /^extrastorage:/, /^compactmachines:/])
    e.add('mekanism:cardboard_blacklist', '#misctags:no_moving')
    e.add('misctags:flowers/draconic_flowers', 'minecraft:dragon_egg')
    e.add('misctags:flowers/end_flowers', ['minecraft:chorus_flower', 'minecraft:chorus_plant'])
    e.add('misctags:flowers/forest_flowers', ['#minecraft:flowers', 'minecraft:sweet_berry_bush'])
    e.add('misctags:flowers/glowing_flowers', ['minecraft:shroomlight', 'minecraft:glowstone', 'minecraft:redstone_lamp'])
    e.add('misctags:flowers/nether_flowers', ['minecraft:crimson_fungus', 'minecraft:warped_fungus', 'minecraft:nether_wart', 'minecraft:crimson_roots', 'minecraft:warped_roots', 'minecraft:weeping_vines', 'minecraft:twisting_vines'])
    e.add('misctags:flowers/swamp_flowers', ['minecraft:lily_pad', 'minecraft:sugar_cane', 'minecraft:brown_mushroom', 'minecraft:red_mushroom'])
    e.add('misctags:flowers/wither_flowers', 'minecraft:wither_rose')
    e.add('forge:mushroom_caps/brown_mushroom', 'minecraft:brown_mushroom_block')
    e.add('forge:mushroom_caps/red_mushroom', 'minecraft:red_mushroom_block')
    e.add('forge:mushroom_stems/mushroom', 'minecraft:mushroom_stem')
    e.add('forge:mushroom_caps', ['#forge:mushroom_caps/red_mushroom', '#forge:mushroom_caps/brown_mushroom'])
    e.add('forge:mushroom_stems', '#forge:mushroom_stems/mushroom')
    e.add('forge:mushrooms', ['minecraft:warped_fungus', 'minecraft:crimson_fungus'])
    e.add('cyclic:scythe_brush', '#minecraft:flowers')

    e.add('mcwwindows:window', '/mcwwindows:.+_win/')
    e.add('misctags:concrete', '/minecraft:.+_concrete/')

    //#region removals
    e.removeAll('minecraft:enderman_holdable')
})

//FLUID TAGS
//onEvent('fluid.tags', e => {
//    e.add('forge:experience', 'witherutilsexp:witherxp')
//})
